<?php

namespace App\Http\Controllers\Guest;
use Cart;
use Cookie;
use Session;
use App\Product;
use App\Order;
use App\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class GuestCtrl extends Controller
{
    
    public function test(Request $request){
        //Cookie::make("test", "rest");

        //return $request->cookie('test');
        
        $cart = [
            1 => [
                "id" => 1,
                "name" => "test",
                "price" => "10",
                "qty" => "3",
                "total" => "3"
            ]
        ];
        //Session::put('userprofile_data',"product");
       // return $cart[1];
        return Session::all();
    }

    public function auth(Request $request){
        return $request->all();

        if (Auth::attempt(['email' => $request->email, 'password' =>$request->password])) {
        
            return Auth::user();
        }
    
        return $request->email;
    }

    public function index(){
        return view("guest.welcome");
    }

    public function products(){
        $products = Product::all();
        
        return view('guest.products')->with(compact('products'));
    }

    public function product($id){
        // Cart::store('usernamex');

        // // To store a cart instance named 'wishlist'
        // Cart::instance('wishlist')->store('usernamex');




        $product = Product::find($id);
        return view("guest.product")->with(compact('product'));
    }

    public function about(){
        return view("guest.about");
    }

    public function contact(){
        return view("guest.contact");
    }
}
