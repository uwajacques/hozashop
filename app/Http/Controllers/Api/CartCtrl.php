<?php

namespace App\Http\Controllers\Api;


use Session;
use App\Product;
use App\Cart;
use \App\Cart_Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartCtrl extends Controller
{
    public function getItem(Request $request){

        $cart = Cart::where('cart_id',  $request->cart_id)->first();

        return $cart->products ?  $cart->products : [];
    }

    public function addItem(Request $request){

        $cart = Cart::where('cart_id', $request->cart_id)->first();
        
        if($cart){
            $c_p = Cart_Product::where('cart_id', $cart->id)->where('product_id' ,$request->product_id)->first();
            if($c_p){
                $c_p->qty += 1;
                $c_p->save();
            }else{
                $c_p = new Cart_Product;
                $c_p->cart_id = $cart->id;
                $c_p->product_id = $request->product_id;
                $c_p->qty = 1;
                $c_p->save();
            }
        }else{
            $c = new Cart;
            $c->cart_id = $request->cart_id;
            $c->customer = 'Customer';
            $c->save();

            $c_p = new Cart_Product;
            $c_p->cart_id = $cart->id;
            $c_p->product_id = $request->product_id;
            $c_p->qty = 1;
            $c_p->save();
        }

        $product = Product::find($request->product_id);

        if(!$cart){
            return  [
                'success' => false,
                'item' => $product
            ];
        }
        
        return  [
            'success' => true,
            'item' => $product
        ];
    }

    public function removeItem(Request $request){
        $product = Product::find($request->id);

   
        return  [
            'success' => true,
            'item' => $product
        ];
    }
}
