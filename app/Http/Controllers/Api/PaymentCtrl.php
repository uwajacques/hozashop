<?php

namespace App\Http\Controllers\Api;

use \App\User;
use \Braintree_Customer;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;


class PaymentCtrl extends Controller
{
    
    public function token(){
        return \Braintree\ClientToken::generate();
    }
    
    public function payment(Request $request){

       $payload = $request->get('payload');
       
       if(empty($payload['nonce'])){
           return false;
       }

        $payment_method_nonce = $payload['nonce'];

        $result = \Braintree_Transaction::sale([
            'amount' =>  $request->total,
            'paymentMethodNonce' => $payment_method_nonce,
            'options' => [
              'submitForSettlement' => True
            ]
        ]);

        if ($result->success) { 
            return [
                    'success' => true,
                    'content' => []
                ];
        }
        
        return [
            'success' => false,
            'content' => []
        ];
    }
}
