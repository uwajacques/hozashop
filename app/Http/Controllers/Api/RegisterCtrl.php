<?php

namespace App\Http\Controllers\Api;

use \App\User;
// use \App\Business;
use \App\Address;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class RegisterCtrl extends Controller
{
    
    public function register(Request $request){

        try {
            $user =  User::where('email', $request->data['email'])->firstOrFail();
            $userFound = true;
        } catch ( Exception $e) {
            $userFound = false;
        }

        if($userFound){
            $user->first_name = $request->data['first_name'];
            $user->last_name = $request->data['last_name'];
            $user->email = $request->data['email'];
            $user->phone = $request->data['phone'];
            $user->user_type = !empty($request->data['company']) ? 'business' : 'customer';
            $user->save();
            return $user;
        }else{
            return 'failed';
        }

        try {
            $user = new User;
            $user->first_name = $request->data['first_name'];
            $user->last_name = $request->data['last_name'];
            $user->email = $request->data['email'];
            $user->phone = $request->data['phone'];
            $user->user_type = !empty($request->data['company']) ? 'business' : 'customer';
           return  $user->save();
           
        } catch (Exception $e) {
            return  $e; // 'failed';
        }
            

        
            $address = new Address;
            $address->building_number = $request->data['building_number'];
            $address->address1 =  $request->data['address1'];
            $address->address2 =  $request->data['address2'];
            $address->postcode =  $request->data['postcode'];
            $address->city =  $request->data['city'];
            $address->user_id = $user->id;
            $address->save();

            // if(!empty($request->data['company'])){
            //     $business = new Business;
            //     $business->company = $request->company;
            //     $business->vat_number = $request->vat_number;
            //     $business->save();
            // }

            if($user){
                return $request->all();
            }else{
                return 'failled';
            }
    }
}
