<?php

namespace App;

use \App\Address;
use \App\Product;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'hash', 
        'total', 
        'paid', 
        'user_id',
        'address_id'
    ];

    public function address(){
        return $this->belongsTo(Address::class);
    }

    public function products(){
        return $this->belongsToMany(Product::class, 'orders_products')->withPivot('qty');
    }
}
