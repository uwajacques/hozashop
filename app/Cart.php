<?php

namespace App;

use \App\Product;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    
    public function products(){
        return $this->belongsToMany(Product::class, 'cart_product')->withPivot('qty');
    }
}
