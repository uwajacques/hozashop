<?php

namespace App;

use \App\Order;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'user_id', 
        'building_number', 
        'address1', 
        'address2',
        'postcode',
        'city',
        'country'
    ];


    public function order(){
        return $this->hasMany(Order::class);
    }
}
