@extends('layouts.master')

@section('content')
   
   <!-- Start Banner Area -->
	<section class="banner-area organic-banner-area">
		<div class="container">
			<div class="banner-slider organic-banner-slider">
				<div class="item">
					<div class="row height align-items-center">
						<div class="col-md-6">
							<div class="left-content">
								<h1 class="text-white mb-10 text-uppercase">Field Fresh Quality <br> Organic Raw Strawberry</h1>
								<p class="text-white">The purposes of food are to promote growth, to supply force and heat, and to furnish material to repair the waste which is constantly.</p>
								<div class="bottom d-flex align-items-center">
									<div class="price"><span class="lnr lnr-tag"></span> $119</div>
									<a href="02-11-product-details.php" target="_blank" class="cart-btn d-flex flex-column"><span class="cart">Add to Cart</span><span class="lnr lnr-cart"></span></a>
								</div>
							</div>
						</div>
						<div class="col-md-6 d-flex justify-content-end">
							<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/b1.png" alt="" class="img-fluid">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row height align-items-center">
						<div class="col-md-6">
							<div class="left-content">
								<h1 class="text-white mb-10 text-uppercase">Perfectly Matured <br> Grapes from our garden</h1>
								<p class="text-white">The purposes of food are to promote growth, to supply force and heat, and to furnish material to repair the waste which is constantly.</p>
								<div class="bottom d-flex align-items-center">
									<div class="price"><span class="lnr lnr-tag"></span> $520</div>
									<a href="02-11-product-details.php" target="_blank" class="cart-btn d-flex flex-column"><span class="cart">Add to Cart</span><span class="lnr lnr-cart"></span></a>
								</div>
							</div>
						</div>
						<div class="col-md-6 d-flex justify-content-end">
							<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/b2.png" alt="" class="img-fluid">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row height align-items-center">
						<div class="col-md-6">
							<div class="left-content">
								<h1 class="text-white mb-10 text-uppercase">in Selling Organic food <br> Pixel store became #1  </h1>
								<p class="text-white">The purposes of food are to promote growth, to supply force and heat, and to furnish material to repair the waste which is constantly.</p>
								<div class="bottom d-flex align-items-center">
									<a href="02-11-product-details.php" target="_blank" class="cart-btn d-flex flex-column"><span class="cart">Shop Now</span><span class="lnr lnr-cart"></span></a>
								</div>
							</div>
						</div>
						<div class="col-md-6 d-flex justify-content-end">
							<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/b3.png" alt="" class="img-fluid">
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row height align-items-center">
						<div class="col-md-6">
							<div class="left-content">
								<h1 class="text-white mb-10 text-uppercase">Reducing Heart Diseases <br> by eating Avocados</h1>
								<p class="text-white">The purposes of food are to promote growth, to supply force and heat, and to furnish material to repair the waste which is constantly.</p>
								<div class="bottom d-flex align-items-center">
									<div class="price"><span class="lnr lnr-tag"></span> $120</div>
									<a href="02-11-product-details.php" target="_blank" class="cart-btn d-flex flex-column"><span class="cart">Add to Cart</span><span class="lnr lnr-cart"></span></a>
								</div>
							</div>
						</div>
						<div class="col-md-6 d-flex justify-content-end">
							<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/b4.png" alt="" class="img-fluid">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->
	<!-- Start products Area -->
	<div class="products-top organic-product-top pt-100">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="single-product-top" style="background: url(img/organic-food/c4.jpg);">
						<div class="content">
							<h4 class="product-title">Chinese Org Honey <br>
							Flat 20% Off</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod.</p>
							<a href="02-11-product-details.php" class="view-btn color-2"><span>Shop Now</span></a>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 middle-section">
					<a href="02-11-product-details.php">
						<div class="single-product-top middle" style="background: url(img/organic-food/c2.jpg);">
							<div class="content">
								<h4 class="product-title">15% Discount on <br> Fresh oranges!</h4>
							</div>
						</div>
					</a>
					<a href="02-11-product-details.php">
						<div class="single-product-top middle mt-30" style="background: url(img/organic-food/c3.jpg);">
							<div class="content">
								<h4 class="product-title text-right">Cherry <br>
									Tomatoes <br>
								10% off	</h4>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="single-product-top" style="background: url(img/organic-food/c4.jpg);">
						<div class="content">
							<h4 class="product-title">Liquid Coconut oil <br>Flat 20% Off</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod.</p>
							<a href="02-02-product-fullwidth-list.php" class="view-btn color-2"><span>Shop Now</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End products Area -->
	<!-- Start Best Seller -->
	<!-- <section class="lattest-product-area section-gap">
		<div class="container">
			<div class="organic-section-title text-center">
				<h3>Latest Products</h3>
			</div>
			<div class="tab-navigation d-flex justify-content-center mt-30">
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li>
						<a class="nav-link active" id="all-tab" data-toggle="tab" href="#all-product" role="tab" aria-controls="all-product" aria-expanded="true">All Products</a>
					</li>
					<li>
						<a class="nav-link" id="fruits-tab" data-toggle="tab" href="#fruits" role="tab" aria-controls="fruits">Fruits</a>
					</li>
					<li>
						<a class="nav-link" id="vegetable-tab" data-toggle="tab" href="#vegetables" role="tab" aria-controls="vegetables">Vegetables</a>
					</li>
					<li>
						<a class="nav-link" id="organic-tab" data-toggle="tab" href="#organic-products" role="tab" aria-controls="organic-products">Organic Products</a>
					</li>
				</ul>
			</div>

			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="all-product" role="tabpanel" aria-labelledby="all-tab">
					<div class="row">
						<div class="col-lg-3 col-md-4 col-sm-6">
							<div class="single-organic-product">
								<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/p1.jpg" alt="" class="img-fluid">
								<h3 class="price">$360.00</h3>
								<span class="text">Pixelstore fresh Blueberry</span>
								<div class="bottom d-flex align-items-center">
									<a href="#"><span class="lnr lnr-heart"></span></a>
									<a href="#"><span class="lnr lnr-layers"></span></a>
									<a href="#"><span class="lnr lnr-cart"></span></a>
									<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6">
							<div class="single-organic-product">
								<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/p2.jpg" alt="" class="img-fluid">
								<h3 class="price">$360.00</h3>
								<span class="text">Pixelstore fresh Blueberry</span>
								<div class="bottom d-flex align-items-center">
									<a href="#"><span class="lnr lnr-heart"></span></a>
									<a href="#"><span class="lnr lnr-layers"></span></a>
									<a href="#"><span class="lnr lnr-cart"></span></a>
									<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6">
							<div class="single-organic-product">
								<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/p3.jpg" alt="" class="img-fluid">
								<h3 class="price">$360.00</h3>
								<span class="text">Pixelstore fresh Blackberry</span>
								<div class="bottom d-flex align-items-center">
									<a href="#"><span class="lnr lnr-heart"></span></a>
									<a href="#"><span class="lnr lnr-layers"></span></a>
									<a href="#"><span class="lnr lnr-cart"></span></a>
									<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
								</div>
								<div class="discount">60%</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6">
							<div class="single-organic-product">
								<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/p4.jpg" alt="" class="img-fluid">
								<h3 class="price">$360.00</h3>
								<span class="text">Pixelstore fresh Redberry</span>
								<div class="bottom d-flex align-items-center">
									<a href="#"><span class="lnr lnr-heart"></span></a>
									<a href="#"><span class="lnr lnr-layers"></span></a>
									<a href="#"><span class="lnr lnr-cart"></span></a>
									<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
								</div>
								<div class="discount">20%</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6">
							<div class="single-organic-product">
								<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/p5.jpg" alt="" class="img-fluid">
								<h3 class="price">$360.00</h3>
								<span class="text">Pixelstore fresh Bell Pepper</span>
								<div class="bottom d-flex align-items-center">
									<a href="#"><span class="lnr lnr-heart"></span></a>
									<a href="#"><span class="lnr lnr-layers"></span></a>
									<a href="#"><span class="lnr lnr-cart"></span></a>
									<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6">
							<div class="single-organic-product">
								<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/p6.jpg" alt="" class="img-fluid">
								<h3 class="price">$360.00</h3>
								<span class="text">Pixelstore fresh Cabbages</span>
								<div class="bottom d-flex align-items-center">
									<a href="#"><span class="lnr lnr-heart"></span></a>
									<a href="#"><span class="lnr lnr-layers"></span></a>
									<a href="#"><span class="lnr lnr-cart"></span></a>
									<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6">
							<div class="single-organic-product">
								<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/p7.jpg" alt="" class="img-fluid">
								<h3 class="price">$360.00</h3>
								<span class="text">Pixelstore Cherry Tomatoes</span>
								<div class="bottom d-flex align-items-center">
									<a href="#"><span class="lnr lnr-heart"></span></a>
									<a href="#"><span class="lnr lnr-layers"></span></a>
									<a href="#"><span class="lnr lnr-cart"></span></a>
									<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-6">
							<div class="single-organic-product">
								<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/p8.jpg" alt="" class="img-fluid">
								<h3 class="price">$360.00</h3>
								<span class="text">Pixelstore fresh Brocoli</span>
								<div class="bottom d-flex align-items-center">
									<a href="#"><span class="lnr lnr-heart"></span></a>
									<a href="#"><span class="lnr lnr-layers"></span></a>
									<a href="#"><span class="lnr lnr-cart"></span></a>
									<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
								</div>
							</div>
						</div>
					</div>
					<div class="load-product">
						<div class="row">
							<div class="col-lg-3 col-md-4 col-sm-6">
								<div class="single-organic-product">
									<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/p2.jpg" alt="" class="img-fluid">
									<h3 class="price">$360.00</h3>
									<span class="text">Pixelstore fresh Blueberry</span>
									<div class="bottom d-flex align-items-center">
										<a href="#"><span class="lnr lnr-heart"></span></a>
										<a href="#"><span class="lnr lnr-layers"></span></a>
										<a href="#"><span class="lnr lnr-cart"></span></a>
										<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-4 col-sm-6">
								<div class="single-organic-product">
									<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/p3.jpg" alt="" class="img-fluid">
									<h3 class="price">$360.00</h3>
									<span class="text">Pixelstore fresh Blackberry</span>
									<div class="bottom d-flex align-items-center">
										<a href="#"><span class="lnr lnr-heart"></span></a>
										<a href="#"><span class="lnr lnr-layers"></span></a>
										<a href="#"><span class="lnr lnr-cart"></span></a>
										<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
									</div>
									<div class="discount">60%</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-4 col-sm-6">
								<div class="single-organic-product">
									<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/p4.jpg" alt="" class="img-fluid">
									<h3 class="price">$360.00</h3>
									<span class="text">Pixelstore fresh Redberry</span>
									<div class="bottom d-flex align-items-center">
										<a href="#"><span class="lnr lnr-heart"></span></a>
										<a href="#"><span class="lnr lnr-layers"></span></a>
										<a href="#"><span class="lnr lnr-cart"></span></a>
										<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
									</div>
									<div class="discount">20%</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-4 col-sm-6">
								<div class="single-organic-product">
									<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/p5.jpg" alt="" class="img-fluid">
									<h3 class="price">$360.00</h3>
									<span class="text">Pixelstore fresh Bell Pepper</span>
									<div class="bottom d-flex align-items-center">
										<a href="#"><span class="lnr lnr-heart"></span></a>
										<a href="#"><span class="lnr lnr-layers"></span></a>
										<a href="#"><span class="lnr lnr-cart"></span></a>
										<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-4 col-sm-6">
								<div class="single-organic-product">
									<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/p6.jpg" alt="" class="img-fluid">
									<h3 class="price">$360.00</h3>
									<span class="text">Pixelstore fresh Cabbages</span>
									<div class="bottom d-flex align-items-center">
										<a href="#"><span class="lnr lnr-heart"></span></a>
										<a href="#"><span class="lnr lnr-layers"></span></a>
										<a href="#"><span class="lnr lnr-cart"></span></a>
										<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-4 col-sm-6">
								<div class="single-organic-product">
									<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/p7.jpg" alt="" class="img-fluid">
									<h3 class="price">$360.00</h3>
									<span class="text">Pixelstore Cherry Tomatoes</span>
									<div class="bottom d-flex align-items-center">
										<a href="#"><span class="lnr lnr-heart"></span></a>
										<a href="#"><span class="lnr lnr-layers"></span></a>
										<a href="#"><span class="lnr lnr-cart"></span></a>
										<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-4 col-sm-6">
								<div class="single-organic-product">
									<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/p8.jpg" alt="" class="img-fluid">
									<h3 class="price">$360.00</h3>
									<span class="text">Pixelstore fresh Brocoli</span>
									<div class="bottom d-flex align-items-center">
										<a href="#"><span class="lnr lnr-heart"></span></a>
										<a href="#"><span class="lnr lnr-layers"></span></a>
										<a href="#"><span class="lnr lnr-cart"></span></a>
										<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="text-center">
						<a href="#" class="view-btn load-more-btn color-2 mt-50"><span>Load More</span></a>
					</div>
				</div>
				<div class="tab-pane fade" id="fruits" role="tabpanel" aria-labelledby="fruits-tab">
					<div class="row">
						<div class="col-lg-3 col-md-4 col-sm-6">
						<div class="single-organic-product">
							<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/p1.jpg" alt="" class="img-fluid">
							<h3 class="price">$360.00</h3>
							<span class="text">Pixelstore fresh Blueberry</span>
							<div class="bottom d-flex align-items-center">
								<a href="#"><span class="lnr lnr-heart"></span></a>
								<a href="#"><span class="lnr lnr-layers"></span></a>
								<a href="#"><span class="lnr lnr-cart"></span></a>
								<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
							</div>
							<div class="discount">60%</div>
						</div>
					</div>
					</div>
				</div>
				<div class="tab-pane fade" id="vegetables" role="tabpanel" aria-labelledby="vegetable-tab">
					<div class="row">
						<div class="col-lg-3 col-md-4 col-sm-6">
						<div class="single-organic-product">
							<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/p1.jpg" alt="" class="img-fluid">
							<h3 class="price">$360.00</h3>
							<span class="text">Pixelstore fresh Blueberry</span>
							<div class="bottom d-flex align-items-center">
								<a href="#"><span class="lnr lnr-heart"></span></a>
								<a href="#"><span class="lnr lnr-layers"></span></a>
								<a href="#"><span class="lnr lnr-cart"></span></a>
								<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
							</div>
							<div class="discount">60%</div>
						</div>
					</div>
					</div>
				</div>
				<div class="tab-pane fade" id="organic-products" role="tabpanel" aria-labelledby="organic-tab">
					<div class="row">
						<div class="col-lg-3 col-md-4 col-sm-6">
							<div class="single-organic-product">
								<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/p1.jpg" alt="" class="img-fluid">
								<h3 class="price">$360.00</h3>
								<span class="text">Pixelstore fresh Blueberry</span>
								<div class="bottom d-flex align-items-center">
									<a href="#"><span class="lnr lnr-heart"></span></a>
									<a href="#"><span class="lnr lnr-layers"></span></a>
									<a href="#"><span class="lnr lnr-cart"></span></a>
									<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
								</div>
								<div class="discount">60%</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> -->
	<!-- End Best Seller -->
	<!-- Start Banner Area -->
	<br>
	<br>
	<br>
	<br>
	<section class="banner-area middle organic-banner-area">
		<div class="container">
			<div class="banner-slider organic-middle-slider">
				<div class="item">
					<div class="row height align-items-center">
						<div class="col-md-6 text-center">
							<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/m1.png" alt="" class="img-fluid">
						</div>
						<div class="col-md-6">
							<div class="left-content text-center">
								<p class="text-white">Organic food</p>
								<h3 class="text-white mb-10 text-uppercase">100% Pure Coconut Chips</h3>
								<p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								<h4 class="h1 text-white text-uppercase">15% off</h4>
								<div class="bottom d-flex align-items-center justify-content-center mt-20">
									<a href="02-11-product-details.php" class="cart-btn d-flex flex-column"><span class="cart">Shop Now</span><span class="lnr lnr-cart"></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row height align-items-center">
						<div class="col-md-6 text-center">
							<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/m1.png" alt="" class="img-fluid">
						</div>
						<div class="col-md-6">
							<div class="left-content text-center">
								<p class="text-white">Organic food</p>
								<h3 class="text-white mb-10 text-uppercase">100% Pure Coconut Chips</h3>
								<p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								<h4 class="h1 text-white text-uppercase">15% off</h4>
								<div class="bottom d-flex align-items-center justify-content-center mt-20">
									<a href="02-11-product-details.php" class="cart-btn d-flex flex-column"><span class="cart">Shop Now</span><span class="lnr lnr-cart"></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="row height align-items-center">
						<div class="col-md-6 text-center">
							<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/m1.png" alt="" class="img-fluid">
						</div>
						<div class="col-md-6">
							<div class="left-content text-center">
								<p class="text-white">Organic food</p>
								<h3 class="text-white mb-10 text-uppercase">100% Pure Coconut Chips</h3>
								<p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								<h4 class="h1 text-white text-uppercase">15% off</h4>
								<div class="bottom d-flex align-items-center justify-content-center mt-20">
									<a href="02-11-product-details.php" class="cart-btn d-flex flex-column"><span class="cart">Shop Now</span><span class="lnr lnr-cart"></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->
	<!-- Start Product Carousel Area -->
	<!-- <div class="organic-product-area pt-100 pb-100">
		<div class="container">
		<div class="organic-section-title text-center">
			<h3>Featured Products</h3>
		</div>
		<div class="organic-product-carousel">
			<div class="single-organic-product">
				<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/f1.jpg" alt="" class="img-fluid">
				<h3 class="price">$360.00</h3>
				<span class="text">Pixelstore fresh piecherry</span>
				<div class="bottom d-flex align-items-center">
					<a href="#"><span class="lnr lnr-heart"></span></a>
					<a href="#"><span class="lnr lnr-layers"></span></a>
					<a href="02-11-product-details.php"><span class="lnr lnr-cart"></span></a>
					<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
				</div>
			</div>
			<div class="single-organic-product">
				<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/f2.jpg" alt="" class="img-fluid">
				<h3 class="price">$360.00</h3>
				<span class="text">Pixelstore fresh Blueberry</span>
				<div class="bottom d-flex align-items-center">
					<a href="#"><span class="lnr lnr-heart"></span></a>
					<a href="#"><span class="lnr lnr-layers"></span></a>
					<a href="02-11-product-details.php"><span class="lnr lnr-cart"></span></a>
					<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
				</div>
			</div>
			<div class="single-organic-product">
				<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/f3.jpg" alt="" class="img-fluid">
				<h3 class="price">$360.00 <del>$560.00</del></h3>
				<span class="text">Pixelstore Cherry Carrots</span>
				<div class="bottom d-flex align-items-center">
					<a href="#"><span class="lnr lnr-heart"></span></a>
					<a href="#"><span class="lnr lnr-layers"></span></a>
					<a href="02-11-product-details.php"><span class="lnr lnr-cart"></span></a>
					<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
				</div>
				<div class="discount">60%</div>
			</div>
			<div class="single-organic-product">
				<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/f4.jpg" alt="" class="img-fluid">
				<h3 class="price">$360.00</h3>
				<span class="text">Pixelstore fresh Beans</span>
				<div class="bottom d-flex align-items-center">
					<a href="#"><span class="lnr lnr-heart"></span></a>
					<a href="#"><span class="lnr lnr-layers"></span></a>
					<a href="02-11-product-details.php"><span class="lnr lnr-cart"></span></a>
					<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
				</div>
				<div class="discount">20%</div>
			</div>
		</div>
	</div>
	</div> -->
	<!-- End Product Carousel Area -->
	
	<!-- Start Count Down Area -->
	<div class="countdown-area">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="countdown-content">
				<div class="title text-center">
					<h3 class="mb-10">Big Sale on Thursday!</h3>
					<p>Get 25% off for every $100 Shopping. Get rady for the blast!</p>
				</div>
			</div>
			<div class="row align-items-center">
				<div class="col-xl-4 col-lg-4"></div>
				<div class="col-xl-6 col-lg-7">
					<div class="countdown d-flex justify-content-center justify-content-md-end" id="js-countdown">
						<div class="countdown-item">
							<div class="countdown-timer js-countdown-days time" aria-labelledby="day-countdown">

							</div>

							<div class="countdown-label" id="day-countdown">Days</div>
						</div>

						<div class="countdown-item">
							<div class="countdown-timer js-countdown-hours" aria-labelledby="hour-countdown">

							</div>

							<div class="countdown-label" id="hour-countdown">Hours</div>
						</div>

						<div class="countdown-item">
							<div class="countdown-timer js-countdown-minutes" aria-labelledby="minute-countdown">

							</div>

							<div class="countdown-label" id="minute-countdown">Minutes</div>
						</div>

						<div class="countdown-item">
							<div class="countdown-timer js-countdown-seconds" aria-labelledby="second-countdown">

							</div>

							<div class="countdown-label text" id="second-countdown">Seconds</div>
						</div>
						<a href="#" class="view-btn color-2"><span>Shop Now</span></a>
						<img src="http://pixelcoder.net/html/pixelstore/img/organic-food/cd.png" class="img-fluid cd-img" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Count Down Area -->
	<!-- Start Most Search Product Area -->
	<!-- <section class="section-half">
		<div class="container">
			<div class="organic-section-title text-center">
				<h3>Most Searched Products</h3>
			</div>
			<div class="row mt-30">
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="http://pixelcoder.net/html/pixelstore/img/organic-food/mp1.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Blueberry</a>
							<div class="price"><span class="lnr lnr-tag"></span> $240.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="http://pixelcoder.net/html/pixelstore/img/organic-food/mp2.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Cabbage</a>
							<div class="price"><span class="lnr lnr-tag"></span> $189.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="http://pixelcoder.net/html/pixelstore/img/organic-food/mp3.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Raspberry</a>
							<div class="price"><span class="lnr lnr-tag"></span> $189.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="http://pixelcoder.net/html/pixelstore/img/organic-food/mp4.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Kiwi</a>
							<div class="price"><span class="lnr lnr-tag"></span> $189.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="http://pixelcoder.net/html/pixelstore/img/organic-food/mp5.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore Bell Pepper</a>
							<div class="price"><span class="lnr lnr-tag"></span> $120.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="http://pixelcoder.net/html/pixelstore/img/organic-food/mp6.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Blackberry</a>
							<div class="price"><span class="lnr lnr-tag"></span> $120.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="http://pixelcoder.net/html/pixelstore/img/organic-food/mp7.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Brocoli</a>
							<div class="price"><span class="lnr lnr-tag"></span> $120.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="http://pixelcoder.net/html/pixelstore/img/organic-food/mp8.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Carrot</a>
							<div class="price"><span class="lnr lnr-tag"></span> $120.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="http://pixelcoder.net/html/pixelstore/img/organic-food/mp9.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Strawberry</a>
							<div class="price"><span class="lnr lnr-tag"></span> $240.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="http://pixelcoder.net/html/pixelstore/img/organic-food/mp10.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Prixma MG2 Light Inkjet</a>
							<div class="price"><span class="lnr lnr-tag"></span> $240.00</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="http://pixelcoder.net/html/pixelstore/img/organic-food/mp11.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Cherry</a>
							<div class="price"><span class="lnr lnr-tag"></span> $240.00 <del>$340.00</del></div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-search-product d-flex">
						<a href="02-11-product-details.php"><img src="http://pixelcoder.net/html/pixelstore/img/organic-food/mp12.jpg" alt=""></a>
						<div class="desc">
							<a href="02-11-product-details.php" class="title">Pixelstore fresh Beans</a>
							<div class="price"><span class="lnr lnr-tag"></span> $240.00 <del>$340.00</del></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> -->
	<!-- End Most Search Product Area -->
	<!-- Start Subscribe Area -->
	<!-- <section class="subscription-area">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="subscription-head text-center">
						<h3 class="mb-20">Never want to miss a trend or promotion again?</h3>
						<p><span>Get a 10% discount</span> voucher for your next purchase & receive exclusive offers and trend news.</p>
					</div>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-lg-6 col-md-10">
					<div id="mc_embed_signup">
						<form target="_blank" novalidate action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&id=92a4423d01" method="get" class="subscription relative">
							<input type="email" name="EMAIL" placeholder="Enter your email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your email address'" required>
							<div style="position: absolute; left: -5000px;">
								<input type="text" name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="">
							</div>
							<button type="submit" class="submit-btn color-2" name="subscribe">Subscribe</button>
							<div class="info"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section> -->
	<!-- End Subscribe Area -->
@endsection 