@extends('layouts.master')

@section('content')

<!-- End Header Area -->	<!-- Start Banner Area -->
<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center">
				<div class="col-first">
					<h1>Product Details</h1>
				</div>
				<div class="col-second">
					<p></p>
				</div>
				<div class="col-third">
					<nav class="d-flex align-items-center justify-content-end">
						<a href="/">Home<i class="fa fa-caret-right" aria-hidden="true"></i>
					</a>
						<a href="/products">Products<i class="fa fa-caret-right" aria-hidden="true"></i></a>
						<a href="">Product Details</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->
	<!-- Start Product Details -->
	<div class="container">
		<div class="product-quick-view">
			<div class="row align-items-center">
				<div class="col-lg-6">
					<div class="quick-view-carousel-details">
						<div class="item" style="background: url(/img/organic-food/q1.jpg);">

						</div>
						<div class="item" style="background: url(/img/organic-food/q1.jpg);">

						</div>
						<div class="item" style="background: url(/img/organic-food/q1.jpg);">

						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="quick-view-content">
						<div class="top">
							<h3 class="head">{{$product->product_name}}</h3>
							<div class="price d-flex align-items-center"><span class="lnr lnr-tag"></span> <span class="ml-10">£{{$product->price}}</span></div>
							<div class="available">Availibility: <span>In Stock</span></div>
						</div>
						<div class="middle">
							<p class="content">{{$product->product_description}}</p>
							<a href="#more-details" class="view-full">View full Details <span class="lnr lnr-arrow-right"></span></a>
						</div>
						<div class="bottom">
							<div class="quantity-container d-flex align-items-center mt-15">
								Quantity:
								<input type="text" class="quantity-amount ml-15" value="1" />
								<div class="arrow-btn d-inline-flex flex-column">
									<button class="increase arrow" type="button" title="Increase Quantity"><span class="lnr lnr-chevron-up"></span></button>
									<button class="decrease arrow" type="button" title="Decrease Quantity"><span class="lnr lnr-chevron-down"></span></button>
								</div>

							</div>
							<div class="d-flex mt-20">
								<a href="#" class="view-btn color-2"><span>Add to Cart</span></a>
								<a href="#" class="like-btn"><span class="lnr lnr-layers"></span></a>
								<a href="#" class="like-btn"><span class="lnr lnr-heart"></span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container" id="more-details">
		<div class="details-tab-navigation d-flex justify-content-center mt-30">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li>
					<a class="nav-link" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-expanded="true">Description</a>
				</li>
				<li>
					<a class="nav-link" id="specification-tab" data-toggle="tab" href="#specification" role="tab" aria-controls="specification">Specification</a>
				</li>
			</ul>
		</div>
		<div class="tab-content" id="myTabContent">
			<div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description">
				<div class="description">
					<p>Beryl Cook is one of Britain’s most talented and amusing artists .Beryl’s pictures feature women of all shapes and sizes enjoying themselves .Born between the two world wars, Beryl Cook eventually left Kendrick School in Reading at the age of 15, where she went to secretarial school and then into an insurance office. After moving to London and then Hampton, she eventually married her next door neighbour from Reading, John Cook. He was an officer in the Merchant Navy and after he left the sea in 1956, they bought a pub for a year before John took a job in Southern Rhodesia with a motor company. Beryl bought their young son a box of watercolours, and when showing him how to use it, she decided that she herself quite enjoyed painting. John subsequently bought her a child’s painting set for her birthday and it was with this that she produced her first significant work, a half-length portrait of a dark-skinned lady with a vacant expression and large drooping breasts. It was aptly named ‘Hangover’ by Beryl’s husband and still hangs in their house today</p>
					<p>It is often frustrating to attempt to plan meals that are designed for one. Despite this fact, we are seeing more and more recipe books and Internet websites that are dedicated to the act of cooking for one. Divorce and the death of spouses or grown children leaving for college are all reasons that someone accustomed to cooking for more than one would suddenly need to learn how to adjust all the cooking practices utilized before into a streamlined plan of cooking that is more efficient for one person creating less waste. The mission</p>
				</div>
			</div>
			<div class="tab-pane fade" id="specification" role="tabpanel" aria-labelledby="specification">
				<div class="specification-table">
					<div class="single-row">
						<span>Width</span>
						<span>128mm</span>
					</div>
					<div class="single-row">
						<span>Height</span>
						<span>508mm</span>
					</div>
					<div class="single-row">
						<span>Depth</span>
						<span>85mm</span>
					</div>
					<div class="single-row">
						<span>Weight</span>
						<span>52gm</span>
					</div>
					<div class="single-row">
						<span>Quality checking</span>
						<span>Yes</span>
					</div>
					<div class="single-row">
						<span>Freshness Duration</span>
						<span>03days</span>
					</div>
					<div class="single-row">
						<span>When packeting</span>
						<span>Without touch of hand</span>
					</div>
					<div class="single-row">
						<span>Each Box contains</span>
						<span>60pcs</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Product Details -->

	<br>
	<br>
	<br>
	<br>
    @endsection 