@extends('layouts.master')

@section('content')

<!-- End Header Area -->	<!-- Start Banner Area -->
	<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center">
				<div class="col-first">
					<h1>Products </h1>
				</div>
				<div class="col-second">
					<p>  </p>
				</div>
				<div class="col-third">
					<nav class="d-flex align-items-center justify-content-end">
						<a href="/">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
						<a href="#">Producs</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->


	<!-- Start Best Seller -->
	<section class="lattest-product-area pb-100">
		<div class="container">
			<div class="row">
			@foreach($products as $product)
				<div class="col-lg-3 col-md-4 col-sm-6">
					<div class="single-organic-product">
						<img src="img/organic-food/p1.jpg" alt="" class="img-fluid">
						<h3 class="price">{{$product->price}}</h3>
						<span class="text">{{$product->product_name}}</span>
						<div class="bottom d-flex align-items-center" style="display: none;"> 
							<a href="/product/{{$product->id}}"><span class="lnr lnr-list"></span></a>
							<a class="add-to-cart"  data-id="{{$product->id}}"><span class="lnr lnr-cart"></span></a>
							<a href="#" data-toggle="modal" data-target="#exampleModal"><span class="lnr lnr-frame-expand"></span></a>
						</div>
						<div class="discount">20%</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</section>
	<!-- End Best Seller -->
	<!-- Start Filter Bar -->
	<div class="container">
		<div class="filter-bar d-flex flex-wrap align-items-center">
			<div class="sorting">
				<select>
					<option value="1">Default sorting</option>
					<option value="1">Default sorting</option>
					<option value="1">Default sorting</option>
				</select>
			</div>
			<div class="sorting">
				<select>
					<option value="1">Show 12</option>
					<option value="1">Show 12</option>
					<option value="1">Show 12</option>
				</select>
			</div>
			<p class="mb-0 mr-auto">Showing 1-12 of 57 results</p>
			<div class="pagination">
				<a href="#" class="prev-arrow"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
				<a href="#" class="active">1</a>
				<a href="#">2</a>
				<a href="#">3</a>
				<a href="#" class="dot-dot"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
				<a href="#">6</a>
				<a href="#" class="next-arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
@endsection 