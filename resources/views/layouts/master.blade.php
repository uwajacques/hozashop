<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon-->
        <!-- <link rel="shortcut icon" href="img/logo/favicon.png"> -->
        <link rel="shortcut icon" href="img/logo/fav2.png">
        <!-- Author Meta -->
        <meta name="author" content="CodePixar">
        <!-- Meta Description -->
        <meta name="description" content="">
        <!-- Meta Keyword -->
        <meta name="keywords" content="">
        <!-- meta character set -->
        <meta charset="UTF-8">
        <!-- Site Title -->
        <title>Hoza</title>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="https://fonts.googleapis.com/css?family=Maven+Pro:400,500,700" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <!--
        CSS
        ============================================= -->
        <!-- Styles -->
        <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('/css/linearicons.css') }}"> 
        <link rel="stylesheet" href="{{ asset('/css/owl.carousel.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/magnific-popup.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }} ">
        <link rel="stylesheet" href="{{ asset('/css/nice-select.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/animate.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/nouislider.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/slicknav.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/main.cs') }}s">

        @if( ! empty($checkout))
            <script src="https://js.braintreegateway.com/web/dropin/1.9.3/js/dropin.min.js"></script>
        @endif
    </head>
    <body>
        <div id="app">
            @section('header')
               <!-- Strat Header Area -->
                <header class="header-organic-food">
                    <div class="default-header">
                        <div class="sticky-header">
                            <div class="container">
                                <div class="row align-items-center header-middle">
                                    <div class="col-md-3 col-6 order-1 order-md-1">
                                        <a href="index.php"><img src="http://hoza.be/img/logo.jpg?1510955865" alt="" class="main-logo img-fluid"></a>
                                    </div>
                                    <div class="col-lg-6 col-md-7 ml-auto col-12 order-3 order-md-2">
                                        <div class="search-bar d-flex">
                                            <input class="search-input" type="text" placeholder="Search product here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search product here'">
                                            <!-- <div class="dropdown custom-categories-dropdown">
                                                <a class="search-dropdown d-flex align-items-center" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">All categories<span class="lnr lnr-chevron-down"></span></a>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                    <a class="dropdown-item" href="#">Laptops & Computers</a>
                                                    <a class="dropdown-item" href="#">Cameras & Photography</a>
                                                    <a class="dropdown-item" href="#">Smart Phones & Consoles</a>
                                                    <a class="dropdown-item" href="#">TV and Audio</a>
                                                    <a class="dropdown-item" href="#">Gadgets</a>
                                                    <a class="dropdown-item" href="#">Accessories</a>
                                                    <a class="dropdown-item" href="#">Printers and Ink</a>
                                                    <a class="dropdown-item" href="#">Computer Components</a>
                                                    <a class="dropdown-item" href="#">Home Entertainment</a>
                                                </div>
                                            </div> -->
                                            <button class="search-submit"><span class="lnr lnr-magnifier"></span></button>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-2 order-2 col-6 order-md-3 d-flex justify-content-end">
                                    
                                    <div class="shortcut-icon d-flex align-items-center">
                                        @guest
                                            <nav>
                                                <ul class="main-menu" style="margin-right: 40px">
                                                    <li class="drop-menu"><a href="{{ route('login') }}">Login</a></li>
                                                    <li class="drop-menu"><a href="{{ route('register') }}">Register</a></li>
                                                </ul>
                                            </nav>
        
                                            @else
                                            <nav>
                                                <ul class="main-menu">
                                                    <li class="drop-menu"><a href="admin/dashboard"> <span class="lnr lnr-user"></span> {{ Auth::user()->first_name }}</a></li>
                                                    <li class="drop-menu">
                                                        <a href="{{ route('logout') }}"
                                                        onclick="event.preventDefault();
                                                                document.getElementById('logout-form').submit();">
                                                            Logout
                                                        </a>
                                                    </li>
                                                </ul>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </nav>
                                        @endguest
                                    </div>

                                        
                                        <div class="shortcut-icon sticky-shortcut d-flex align-items-center">
                                            <div class="single-shortcut top-cart mr-0">
                                                <span class="lnr lnr-cart"></span>
                                                <span class="sml cart-count">00</span>
                                            </div>
                                            
                                            <div class="mini-cart mini-cart-2">
                                                <div class="mini-border"></div>
                                                <div class="total-amount-sml d-flex justify-content-between">
                                                    <div class="title">
                                                        <h6>My Cart</h6>
                                                        <span class="items-amount" >0</span>
                                                    </div>
                                                    <div class="amount">£<span>00.00</span></div>
                                                </div>
                                                <div class="cart-item-list">
                                                </div>
                                                <div class="proceed-btn"><a href="javascript: void(0)" class="view-btn color-2"><span>Proceed to checkout</span></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="
                                header-bottom row md-down-none no-gutters align-items-center">
                                    <div class="col-lg-8">
                                        <nav>
                                            <ul class="main-menu">
                                                <li class="drop-menu"><a href="/"><span>Home</span></a> </li>
                                                <li class="drop-menu"><a href="/products"><span>Products</span></a></li>
                                                <li class="drop-menu"><a href="/about"><span>About</span></a> </li>
                                                <li class="drop-menu"><a href="/contact"><span>Contact</span></a> </li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <div class="col-lg-4 d-flex justify-content-end">
                                        <div class="shortcut-icon d-flex align-items-center">
                                            <div class="single-shortcut top-cart mr-0">
                                                <span class="lnr lnr-cart"></span>
                                                <span class="xl cart-count">00</span>
                                            </div>
                                            <div class="mini-cart mini-cart-2">
                                                <div class="mini-border"></div>
                                                <div class="total-amount-xl  d-flex justify-content-between">
                                                    <div class="title">
                                                        <h6>My Cart</h6>
                                                        <span class="items-amount" >0</span>
                                                    </div>
                                                    <div class="amount">£<span>00.00</span></div>
                                                </div>
                                                <div class="cart-item-list">
                                                </div>
                                                <div class="proceed-btn"><a href="/cart" class="view-btn color-2"><span>Proceed to checkout</span></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="catagory-wrap">
                                    <div class="all-slick">
                                        <ul class="main-ul" id="menu">
                                        <li class="drop-menu"><a href="/"><span>Home</span></a> </li>
                                        <li class="drop-menu"><a href="/products"><span>Products</span></a></li>
                                        <li class="drop-menu"><a href="/about"><span>About</span></a> </li>
                                        <li class="drop-menu"><a href="/contact"><span>Contact</span></a> </li>
                                        </ul>
                                    </div>
                                </div>				
                            </div>
                        </div>
                    </div>
                </header>
                <!-- End Header Area -->	
            @show
  

            @yield('content')

            @section('footer')
              <!-- Start Footer Area -->
                <footer class="organic-footer">
                    <div class="container">
                        <div class="footer-widget">
                            <div class="row">
                                <!-- <div class="col-lg-3 col-sm-6">
                                    <div class="single-footer-widget">
                                        <img src="http://pixelcoder.net/html/pixelstore/img/logo/f-logo2.png" alt="">
                                        <ul>
                                            <li><a href="#">New Season</a></li>
                                            <li><a href="#">Press</a></li>
                                            <li><a href="#">Investor relations</a></li>
                                            <li><a href="#">Careers</a></li>
                                            <li><a href="#">Terms & Conditions</a></li>
                                            <li><a href="#">Legal</a></li>
                                            <li><a href="#">Withdrawal</a></li>
                                        </ul>
                                    </div>
                                </div> -->
                                <div class="col-lg-4 col-sm-12">
                                    <div class="single-footer-widget">
                                        <h5>INFORMATION</h5>
                                        <ul>
                                            <li><a href="#">Livraison</a></li>
                                            <li><a href="#">Mentions légales</a></li>
                                            <li><a href="#">Conditions d'utilisation</a></li>
                                            <li><a href="#">Qui sommes-nous</a></li>
                                            <li><a href="#">Payement sécurisé</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-sm-12">
                                    <div class="single-footer-widget">
                                        <h5>Mon compte</h5>
                                        <ul>
                                            <li><a href="#">Mes commandes</a></li>
                                            <li><a href="#">Mes avoirs</a></li>
                                            <li><a href="#">Mes adresses</a></li>
                                            <li><a href="#">Mes informations personnelles</a></li>
                            
                                        </ul>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4 col-sm-12">
                                    <div class="single-footer-widget">
                                        <h5>Contactez-nous
                            
                                        </h5>
                                        <ul>
                                            <li><b>HOZA GROUP</b></li>
                                            <li>Rue Laskouter 7 / 23</li>
                                            <li>1120 Bruxelles</li>
                                            <li>Belgique</li>
                                            <li>Tél : +32 483 599 193</li>
                                            <li>e-Mail : info@hoza.be</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="footer-widget-bottom d-flex flex-wrap justify-content-between mt-30">
                                <div class="left d-flex align-items-center mt-20">
                                    <p class="mb-0 mr-10 text-white">We Accept:</p>
                                    <img src="http://pixelcoder.net/html/pixelstore/img/electronics/ac.png" class="img-fluid" alt="">
                                </div>
                                <div class="right d-flex align-items-center mt-20">
                                    <p class="mb-0 mr-10 text-white">Our shipping partners:</p>
                                    <img src="http://pixelcoder.net/html/pixelstore/img/electronics/ac3.png" class="img-fluid" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="footer-bottom d-flex justify-content-between flex-wrap">
                            <p class="copyright-text">Copyright &copy; 2018 <a href="#">Hoza Group</a>. <br> All rights reserved.</p>
                            <div class="footer-social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-youtube"></i></a>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </div>
                        </div>
                    </div>
                </footer>
                <!-- End Footer Area -->
            @show
        </div>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="container relative">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="product-quick-view">
                        <div class="row align-items-center">
                            <div class="col-lg-6">
                                <div class="quick-view-carousel">
                                    <div class="item" style="background: url(/img/organic-food/q1.jpg);">

                                    </div>
                                    <div class="item" style="background: url(/img/organic-food/q1.jpg);">

                                    </div>
                                    <div class="item" style="background: url(/img/organic-food/q1.jpg);">

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="quick-view-content">
                                    <div class="top">
                                        <h3 class="head">Mill Oil 1000W Heater, White</h3>
                                        <div class="price d-flex align-items-center"><span class="lnr lnr-tag"></span> <span class="ml-10">$149.99</span></div>
                                        <div class="category">Category: <span>Household</span></div>
                                        <div class="available">Availibility: <span>In Stock</span></div>
                                    </div>
                                    <div class="middle">
                                        <p class="content">Mill Oil is an innovative oil filled radiator with the most modern technology. If you are looking for something that can make your interior look awesome, and at the same time give you the pleasant warm feeling during the winter.</p>
                                        <a href="#" class="view-full">View full Details <span class="lnr lnr-arrow-right"></span></a>
                                    </div>
                                    <div class="bottom">
                                        <div class="color-picker d-flex align-items-center">Color: 
                                            <span class="single-pick"></span>
                                            <span class="single-pick"></span>
                                            <span class="single-pick"></span>
                                            <span class="single-pick"></span>
                                            <span class="single-pick"></span>
                                        </div>
                                        <div class="quantity-container d-flex align-items-center mt-15">
                                            Quantity:
                                            <input type="text" class="quantity-amount ml-15" value="1" />
                                            <div class="arrow-btn d-inline-flex flex-column">
                                                <button class="increase arrow" type="button" title="Increase Quantity"><span class="lnr lnr-chevron-up"></span></button>
                                                <button class="decrease arrow" type="button" title="Decrease Quantity"><span class="lnr lnr-chevron-down"></span></button>
                                            </div>

                                        </div>
                                        <div class="d-flex mt-20">
                                            <a href="#" class="view-btn color-2"><span>Add to Cart</span></a>
                                            <a href="#" class="like-btn"><span class="lnr lnr-layers"></span></a>
                                            <a href="#" class="like-btn"><span class="lnr lnr-heart"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    
        <form>
  <div id="dropin-container"></div>
</form>

        
    </body>
    <script src="/js/vendor/jquery-2.2.4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
    <script src="/js/vendor/popper.min.js"></script>
    <script src="/js/vendor/bootstrap.min.js"></script>
    <script src="/js/vendor/parsley.min.js"></script>
    <script src="/js/vendor/jquery.nice-select.js"></script>
    <script src="/js/vendor/jquery.ajaxchimp.min.js"></script>
    <script src="/js/vendor/jquery.sticky.js"></script>
    <script src="/js/vendor/jquery.magnific-popup.min.js"></script>
    <script src="/js/vendor/jquery.slicknav.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAA7NBKe7JFmABuLmSSZo4Xv4iGVcaKcXs"></script>
    <script src="/js/vendor/gmap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bluebird/3.5.1/bluebird.min.js"></script>
    <script src="/js/vendor/map-init.js"></script>
    <script src="/js/vendor/owl.carousel.min.js"></script>
    <script src="/js/vendor/nouislider.min.js"></script>
    <script src="/js/vendor/main.js"></script>
    <script src="{{ asset('/js/app.js') }}"></script>

    @if( ! empty($checkout))
    <script>

    $(document).ready(function(){
        
        braintree.dropin.create({
            authorization: '{{Braintree\ClientToken::generate()}}',
            container: '#dropin-container',
            paypal: {
                flow: 'vault'
            }
        }, function (createErr, instance) {
            $('#submit-payment').click( function (e) {
                e.preventDefault();

                var first_name = $(".billing-form .first_name").val();
                var last_name = $(".billing-form .last_name").val();
                var number = $(".billing-form .number").val();
                var email = $(".billing-form .email").val();
                var building_number = $(".billing-form .building_number").val();
                var address1 = $(".billing-form .address1").val();
                var address2 = $(".billing-form .address2").val();
                var postcode = $(".billing-form .postcode").val();
                var city = $(".billing-form .city").val();

                var company = $(".billing-form .company").val();
                var vat_number = $(".billing-form .vat_number").val();

                if($('.business-shopping').is(':checked')){
                    if(company.length == 0){
                        $(".billing-form .company").css('border', '1px solid red')
                        
                    }else{
                        $(".billing-form .company").css('border', '1px solid #eee')
                        
                    }

                    if(vat_number.length == 0){
                        
                        $(".billing-form .vat_number").css('border', '1px solid red')
                    }else{
                        $(".billing-form .vat_number").css('border', '1px solid #eee')
                        
                    }
                }

                if(first_name.length == 0){
                    
                    $(".billing-form .first_name").css('border', '1px solid red')
                }else{
                    $(".billing-form .first_name").css('border', '1px solid #eee')
                    
                }

                if(last_name.length == 0){
                    
                    $(".billing-form .last_name").css('border', '1px solid red')
                }else{
                    $(".billing-form .last_name").css('border', '1px solid #eee')
                    
                }

                if(email.length == 0){
                    
                    $(".billing-form .email").css('border', '1px solid red')
                }else{
                    $(".billing-form .email").css('border', '1px solid #eee')
                    
                }

                if(building_number.length == 0){
                    
                    $(".billing-form .building_number").css('border', '1px solid red')
                }else{
                    $(".billing-form .building_number").css('border', '1px solid #eee')
                    
                }

                if(address1.length == 0){
                    
                    $(".billing-form .address1").css('border', '1px solid red')
                }else{
                    $(".billing-form .address1").css('border', '1px solid #eee')
                    
                }

                if(address2.length == 0){
                    
                    $(".billing-form .address2").css('border', '1px solid red')
                }else{
                    $(".billing-form .address2").css('border', '1px solid #eee')
                    
                }

                if(address2.length == 0){
                    
                    $(".billing-form .address2").css('border', '1px solid red')
                }else{
                    $(".billing-form .address2").css('border', '1px solid #eee')
                    
                }

                if(postcode.length == 0){
                    
                    $(".billing-form .postcode").css('border', '1px solid red')
                }else{
                    $(".billing-form .postcode").css('border', '1px solid #eee')
                    
                }

                if(city.length == 0){
                    
                    $(".billing-form .city").css('border', '1px solid red')
                }else{
                    $(".billing-form .city").css('border', '1px solid #eee')
                    
                }

                var first_name = $(".billing-form .first_name").val();
                var last_name = $(".billing-form .last_name").val();
                var number = $(".billing-form .number").val();
                var email = $(".billing-form .email").val();
                var phone = $(".billing-form .number").val();
                var building_number = $(".billing-form .building_number").val();
                var address1 = $(".billing-form .address1").val();
                var address2 = $(".billing-form .address2").val();
                var postcode = $(".billing-form .postcode").val();
                var city = $(".billing-form .city").val();

                var company = '';
                var vat_number = '';
                
                if($('.business-shopping').is(':checked')){
                    company = $(".billing-form .company").val();
                    vat_number = $(".billing-form .vat_number").val();
                }


                var customer = {
                    first_name: first_name, 
                    last_name: last_name, 
                    email: email, 
                    phone: phone, 
                    building_number: building_number, 
                    address1: address1, 
                    address2: address2, 
                    postcode: postcode, 
                    city: city,
                    company: company,
                    vat_number: vat_number,
                };

                if($('.business-shopping').is(':checked')){
                    if(
                        first_name.length !== 0 && 
                        last_name.length !== 0  &&
                        email.length !== 0  &&
                        building_number.length !== 0  &&
                        address1.length !== 0  &&
                        address2.length !== 0  &&
                        postcode.length !== 0  &&
                        city.length !== 0  && 
                        company.length !== 0 &&   
                        vat_number.length !== 0
                    ){
                        console.log(customer);
                        if(!$('#dropin-container [data-braintree-id]').hasClass('braintree-show-options')){
                            register(customer).then(function(data){
                                console.log('====================================');
                                console.log(data);
                                console.log('====================================');
                                // payNow(createErr, instance)
                            });
                        }else{
                            console.log('====================================');
                            console.log("selelect payment method");
                            console.log('====================================');
                        }
                    }
                }else{
                    if(
                        first_name.length !== 0 && 
                        last_name.length !== 0  &&
                        email.length !== 0  &&
                        building_number.length !== 0  &&
                        address1.length !== 0  &&
                        address2.length !== 0  &&
                        postcode.length !== 0  &&
                        city.length !== 0 
                    ){
                        
                        if(!$('#dropin-container [data-braintree-id]').hasClass('braintree-show-options')){
                            register(customer).then(function(data){
                                console.log('====================================');
                                console.log(data);
                                console.log('====================================');
                            })
                           // payNow(createErr, instance)
                        }else{
                            console.log('====================================');
                            console.log("selelect payment method");
                            console.log('====================================');
                        }
                        
                    }
                }
                
                

            

               
                // instance.requestPaymentMethod(function (requestPaymentMethodErr, payload) {
                //     console.log('====================================');
                //     console.log(payload);
                //     console.log('====================================');

                //     /*
                //     $.ajax({
                //         type: 'POST',
                //         url: "/api/payment",
                //         data: {
                //             payload: payload,
                //             total: {{$total}}
                //         },
                //         success: function (result) {
                //             if(result.success){
                //                 window.location.replace("/confirmation");
                //             }
                //             console.log("result", result)  
                //         }
                //     }); */
                // })  
            });
        });

        function register(data){
            return new Promise(function(resolve, rject){
                $.ajax({
                    type: 'POST',
                    url: "/api/register",
                    data: {
                        data: data
                    },
                    success: function (result) {
                        resolve(result);
                    }
                }); 
            })
            
        }

        function payNow(createErr, instance){
            instance.requestPaymentMethod(function (requestPaymentMethodErr, payload) {
            $.ajax({
                type: 'POST',
                url: "/api/payment",
                data: {
                    payload: payload,
                    total: {{$total}}
                },
                success: function (result) {
                    if(result.success){
                        //window.location.replace("/confirmation");
                    }
                    console.log("result", result)  
                }
            }); 
            })
        }

        
        $(".billing-form .first_name").keyup( function() {
            if( $(".billing-form .first_name").val().length == 0){
                
                $(".billing-form .first_name").css('border', '1px solid red')
            }else{
                
                $(".billing-form .first_name").css('border', '1px solid #eee')
            }
        });

        $(".billing-form .last_name").keyup( function() {
            if( $(".billing-form .last_name").val().length == 0){
                
                $(".billing-form .last_name").css('border', '1px solid red')
            }else{
                
                $(".billing-form .last_name").css('border', '1px solid #eee')
            }
        });

        $(".billing-form .email").keyup( function() {
            if( $(".billing-form .email").val().length == 0 || !validateEmail($(".billing-form .email").val())){
                
                $(".billing-form .email").css('border', '1px solid red')
            }else{
                
                $(".billing-form .email").css('border', '1px solid #eee')
            }
        });

        $(".billing-form .building_number").keyup( function() {
            if( $(".billing-form .building_number").val().length == 0){
                
                $(".billing-form .building_number").css('border', '1px solid red')
            }else{
                
                $(".billing-form .building_number").css('border', '1px solid #eee')
            }
        });

        $(".billing-form .address1").keyup( function() {
            if( $(".billing-form .address1").val().length == 0){
                
                $(".billing-form .address1").css('border', '1px solid red')
            }else{
                
                $(".billing-form .address1").css('border', '1px solid #eee')
            }
        });

        $(".billing-form .address2").keyup( function() {
            if( $(".billing-form .address2").val().length == 0){
                
                $(".billing-form .address2").css('border', '1px solid red')
            }else{
                
                $(".billing-form .address2").css('border', '1px solid #eee')
            }
        });

        $(".billing-form .postcode").keyup( function() {
            if( $(".billing-form .postcode").val().length == 0){
                
                $(".billing-form .postcode").css('border', '1px solid red')
            }else{
                
                $(".billing-form .postcode").css('border', '1px solid #eee')
            }
        });

        $(".billing-form .city").keyup( function() {
            if( $(".billing-form .city").val().length == 0){
                
                $(".billing-form .city").css('border', '1px solid red')
            }else{
                
                $(".billing-form .city").css('border', '1px solid #eee')
            }
        });

        $(".billing-form .company").keyup( function() {
            if($('.business-shopping').is(':checked')){
                if( $(".billing-form .company").val().length == 0){
                    
                    $(".billing-form .company").css('border', '1px solid red')
                }else{
                    
                    $(".billing-form .company").css('border', '1px solid #eee')
                }
            }
        })
        
        $(".billing-form .vat_number").keyup( function() {
            if($(".billing-form .vat_number").val().length == 0){
                
                $(".billing-form .vat_number").css('border', '1px solid red')
            }else{
                
                $(".billing-form .vat_number").css('border', '1px solid #eee')
            }
        })

        
        function validateEmail($email) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailReg.test( $email );
        }
    })
        

    </script>
    @endif

</html>
