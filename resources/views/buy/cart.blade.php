@extends('layouts.master')

@section('content')
<!-- End Header Area -->	<!-- Start Banner Area -->
<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center">
				<div class="col-first">
					<h1>Cart</h1>
				</div>
				<div class="col-second">
					<p>So you have your new digital camera and clicking away to glory anything and everything in sight.</p>
				</div>
				<div class="col-third">
					<nav class="d-flex align-items-center justify-content-end">
						<a href="index.php">Home<i class="fa fa-caret-right" aria-hidden="true"></i></a>
						<a href="#">Cart</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->

	
	<!-- Start Cart Area -->
	<div class="container">
		<div class="cart-title">
			<div class="row">
				<div class="col-md-6">
					<h6 class="ml-15">Product</h6>
				</div>
				<div class="col-md-2">
					<h6>Price</h6>
				</div>
				<div class="col-md-2">
					<h6>Quantity</h6>
				</div>
				<div class="col-md-2">
					<h6>Total</h6>
				</div>
			</div>
		</div>
		@foreach($products as $product)
		<div class="cart-single-item">
			<div class="row align-items-center">
				<div class="col-md-6 col-12">
					<div class="product-item d-flex align-items-center">
						<img src="img/organic-food/ci1.jpg" class="img-fluid" alt="">
						<h6>{{$product->product_name}}</h6>
					</div>
				</div>
				<div class="col-md-2 col-6">
					<div class="price">£{{$product->price}}</div>
				</div>
				<div class="col-md-2 col-6">
					<div class="quantity-container d-flex align-items-center mt-15">
						<input type="text" class="quantity-amount" value="{{$product->pivot->qty}}" />
						<div class="arrow-btn d-inline-flex flex-column">
							<button class="increase arrow" type="button" title="Increase Quantity"><span class="lnr lnr-chevron-up"></span></button>
							<button class="decrease arrow" type="button" title="Decrease Quantity"><span class="lnr lnr-chevron-down"></span></button>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-12">
					<div class="total">£{{$product->price * $product->pivot->qty}}</div>
				</div>
			</div>
		</div>
		@endforeach

		<div class="subtotal-area d-flex align-items-center justify-content-end">
			<div class="title text-uppercase">Subtotal</div>
			<div class="subtotal">£ {{$total}} </div>
		</div>
        <div class="cupon-area d-flex align-items-center justify-content-between flex-wrap">
			<a href="/products" class="view-btn color-2"><span>Continue Shopping </span></a>
            <a href="/checkout/{{$cart_id}}" class="view-btn color-2"><span>Proceed</span></a>
		</div>
	</div>
	<!-- End Cart Area -->
	<br>
	<br>
	<br>
	<br>
	

	
@endsection()