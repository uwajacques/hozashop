@extends('layouts.admin')

@section('content')
 <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
    <h1 class="h2">Products </h1>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
        <a href="new/product" class="btn btn-sm btn-outline-secondary">Add Product </a>
        </div>
    </div>
    </div>

    <!-- <canvas class="my-4" id="myChart" width="900" height="380"></canvas> -->

    <div class="table-responsive">
    <table class="table table-striped table-sm">
        <thead>
        <tr>
            <th style="width: 117px">Code </th>
            <th style="width: 30%">Name</th>
            <th  >Description</th>
            <th style="width: 117px">Price</th>
            <th style="width: 117px">Quantity</th>
            <th style="width: 117px"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
        <tr>
            <td>{{$product->id}}</td>
            <td>{{$product->product_name}}</td>
            <td>{{$product->product_description}}</td>
            <td>{{$product->price}}</td>
            <td>{{$product->stock}}</td>
            <td>
                <a  class="btn btn-primary btn-sm">Delete</a>
                <a href="/admin/update/product/{{$product->id}}" class="btn btn-secondary btn-sm" >Edit</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</main>

@endsection
