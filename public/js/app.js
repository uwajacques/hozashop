// A $( document ).ready() block.
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(function () {
        init()
    });

    $(".add-to-cart").click(function (e) {
        e.preventDefault();

        cart = '';
        if (typeof localStorage.getItem('cart') === "object") {
            localStorage.setItem('cart', Math.floor((Math.random() * 100000) + 200000));
            cart = localStorage.getItem('cart');
        } else {
            cart = localStorage.getItem('cart');
        }

        $(".proceed-btn a").attr("href", "/cart/"+ cart);

        $.ajax({
            type: 'POST',
            url: "/api/cart/add",
            data: {
                product_id: $(this).data('id'),
                cart_id: cart
            },
            success: function (result) {
          
                if (result.success) {
                    let count = parseInt($(".top-cart .xl.cart-count").text()) + 1;
                    let itemsamount = parseInt($(".items-amount").text()) + 1;

                    let item_id = result.item.id;
                    let name = result.item.product_name;
                    let price = result.item.price;
                    let items = $('.item-' + item_id).length;

                    let total_price = parseInt($(".total-amount-xl .amount span").text()) + price;

                    $(".top-cart .xl.cart-count").text(('0' + count).slice(-2)) 
                    $(".top-cart .sml.cart-count").text(('0' + count).slice(-2))

                    $(".total-amount-xl .amount span").text(total_price)
                    $(".total-amount-sml .amount span").text(total_price)

                    $(".items-amount").text(('0' + itemsamount).slice(-2)) 

                    if ($('.item-' + item_id).length !== 0) {

                        let qty = parseInt($('.item-' + item_id).data('qty')) + 1;

                        $('.item-' + item_id).data('qty', qty);

                        $('.item-' + item_id).find('.item_qty').text(qty);
           
                    } else {
                        let item = '<div data-item_id="'+ item_id+'" data-qty="1" class="single-cart-item d-flex item-'+item_id+' justify-content-between align-items-center"> <a href="#"><img src="http://pixelcoder.net/html/pixelstore/img/electronics/ct1.jpg" alt=""></a> <div class="middle"> <h5><a href="#">' + result.item.product_name + '</a></h5> <h6><span class="lnr lnr-tag"></span> £' + price + ' x <span class="item_qty">1</span></h6> </div> <div class="cross"><span class="lnr lnr-cross"></span></div> </div> ';
                        
                        $(".cart-item-list").append(item);
                    }
                }
            }
        });
    });

    $(".business-shopping").click(function (e) {
        //e.preventDefault(); 
        if ($(this).hasClass('no')) {
            $(this).addClass("yes")
            $(this).removeClass("no")
            $('.business-fields').css('display', 'block');
        } else {
            $(this).addClass("no")
            $(this).removeClass("yes")
            $('.business-fields').css('display', 'none');
        }
    })

    // $(".ajax-login-form .login-btn").click(function (e) {
    //     e.preventDefault();
    //    var email = $(".ajax-login-form .email").val()
    //     var password = $(".ajax-login-form .password").val()
       
    //     $.ajax({
    //         type: 'POST',
    //         url: "/auth/login",
    //         data: {
    //             email: email,
    //             password: password
    //         },
    //         success: function (result) {
    //             console.log(result );
    //         }
    //     })
    // })

    
});

//load cart items
function init() {
    var cart = localStorage.getItem('cart')
    $.ajax({
        type: 'POST',
        url: "/api/cart",
        data: {
            cart_id: cart
        },
        success: function (result) {
            if (result.length > 0) {

               var total_item = 0;
               var total_price = 0;

                result.map(function (item) {
                    
                    let elem = '<div data-item_id="'+ item.id +'" data-qty="'+ item.pivot.qty +'" class="single-cart-item d-flex item-'+item.id +' justify-content-between align-items-center"> <a href="#"><img src="http://pixelcoder.net/html/pixelstore/img/electronics/ct1.jpg" alt=""></a> <div class="middle"> <h5><a href="#">' + item.product_name  + '</a></h5> <h6><span class="lnr lnr-tag"></span> £' + item.price + ' x <span class="'+ item.pivot.qty+ '">'+item.pivot.qty+'</span></h6> </div> <div class="cross"><span class="lnr lnr-cross"></span></div> </div> ';

                    $(".cart-item-list").append(elem);

                    total_item =+  total_item + item.pivot.qty;
                    total_price = total_price + (item.price * item.pivot.qty);

                    
                }) 
                $(".amount span").text(total_price.toFixed(2)) 
                $(".top-cart .xl.cart-count").text(('0' + total_item).slice(-2)) 
                $(".top-cart .sml.cart-count").text(('0' + total_item).slice(-2))
                $(".items-amount").text(('0' + total_item).slice(-2)) 
                $(".proceed-btn a").attr("href", "/cart/"+ cart);
            }
        }
    });
}
